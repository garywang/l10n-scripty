#! /bin/bash

podir=${podir:-$PWD/po}
files=$(find . -name XmlMessages.sh)
dirs=$(for i in $files; do dirname "$i"; done | sort -u)
tmpname="$PWD/messages.log"
INTLTOOL_EXTRACT=${INTLTOOL_EXTRACT:-intltool-extract}
INTLTOOL_FLAGS=(-q)
test -z "$VERBOSE" || INTLTOOL_FLAGS=()
XGETTEXT=${XGETTEXT:-xgettext}
# using xgettext 0.15 or later
### TODO what --flags param should be used?
XGETTEXT_FLAGS=(--copyright-holder=This_file_is_part_of_KDE --from-code=UTF-8 -C --kde --msgid-bugs-address=https://bugs.kde.org)
export INTLTOOL_EXTRACT XGETTEXT XGETTEXT_FLAGS

for subdir in $dirs; do
  test -z "$VERBOSE" || echo "Making XML messages in $subdir"
  (cd "$subdir" || echo "Failed to change directory into $subdir"

   if test -f XmlMessages.sh; then
     xml_po_list=$(bash -c ". XmlMessages.sh ; get_files")
     for xml_file_relpath in $xml_po_list; do
       xml_file_po=$(bash -c ". XmlMessages.sh ; po_for_file $xml_file_relpath")
       tags_for_file=$(bash -c ". XmlMessages.sh ; tags_for_file $xml_file_relpath")

       xml_podir=${xml_file_relpath}.podir
       xml_in_file=$xml_podir/$(basename "$xml_file_relpath").in
       if [ ! -e "$xml_podir" ]; then
         mkdir "$xml_podir"
         # intltool-extract does not support message context which we need, so do it one tag at a time plus sed
         for tag in $tags_for_file; do
             sed -e 's/.*lang=.*//g' < "$xml_file_relpath" | sed -r -e "s/(\<\/?)${tag}(\>)/\1_${tag}\2/g" > "${xml_in_file}.${tag}"

             if test -s "${xml_in_file}.${tag}" ; then
               $INTLTOOL_EXTRACT "${INTLTOOL_FLAGS[@]}" --type='gettext/xml' "${xml_in_file}.${tag}"
             else
               echo "Empty preprocessed XML file: $xml_in_file.${tag} !"
             fi
             sed -i  "s/N_(/NC_(\"${tag}\", /" "${xml_in_file}.${tag}.h"
         done
         cat "${xml_in_file}".*.h > "${xml_in_file}.h"

         xmlpot=${podir}/${xml_file_po}t
         $XGETTEXT "${XGETTEXT_FLAGS[@]}" --keyword=NC_:1c,2 -o "$xmlpot" "${xml_in_file}.h"

         rm -rf "$xml_podir"
         $FILLXMLFROMPO "$xml_file_relpath" "$L10NDIR" "$SUBMODULE" "$xml_file_po" "$xmlpot"
         xmllint "$xml_file_relpath" --noout
       else
         echo "$xml_podir exists!"
       fi
     done
   fi
   exit_code=$?
   if test "$exit_code" -ne 0; then
       echo "Bash exit code: $exit_code"
   else
       rm -f rc.cpp
   fi
   ) >& "$tmpname"
   test -s "$tmpname" && { echo "$subdir" ; cat "$tmpname"; }
done

rm -f "$tmpname"
