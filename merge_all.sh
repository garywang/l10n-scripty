# Some description here could be useful...

# Return 0 if no parent dir of the given file path contains ignore-file
noignore() {
    pardir=$1
    while test $pardir != "."; do
        # Next three lines replace pardir=`dirname $pardir`, way too slow.
        pardir_p=$pardir
        pardir=${pardir%/*}
        test $pardir != $pardir_p || pardir=.

        test -f $pardir/no-auto-merge && return 1
    done
    return 0
}

if test -n "$FULLLIST"; then
  list=
  for i in $FULLLIST; do
    list="$list `echo $i | sed -e 's#^templates/\(.*\)\.pot$#\1#'`"
  done
  # echo $list
elif test -z "$LIST"; then
  list=`find templates -name '*.pot' | sed -e 's#^templates/\(.*\)\.pot$#\1#'`
else
  list=
  for i in $LIST; do
    list="$list `find templates -name $i | sed -e 's#^templates/\(.*\)\.pot$#\1#'`"
  done
  echo $list
fi
languages_to_potentially_merge=
for i in *; do
        # "en" is a special language which is only used to hold
        # the plural forms for Qt-based translation files,
        # so its files should not be updated from the templates.
        if [ "$i" = "en" ]; then
                continue
        fi

        if test -d $i/messages -o -d $i/docmessages; then
               languages_to_potentially_merge="$languages_to_potentially_merge $i"
        fi
done
for temp in $list; do
  test -n "$VERBOSE" && echo merging $temp
  languages_to_merge=
  for subdir in $languages_to_potentially_merge; do
    file=$subdir/$temp.po
    test -f $file || continue
    noignore $file || continue
    if test ! -s $file; then
      echo "ERROR: $file is empty!"
    else
      languages_to_merge="$languages_to_merge $subdir"
    fi
  done
  if [ "x$languages_to_merge" != "x" ]; then
    ionice -c 2 -n 7 nice -n 30 parallel msgmerge --previous -q -o {}/$temp.po {}/$temp.po templates/$temp.pot ::: $languages_to_merge
  fi
done
