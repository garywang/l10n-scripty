#! /bin/bash
# kate: space-indent on; indent-width 2; replace-tabs on;

extract_desktop() {
  perl ${dir}/createdesktopcontext.pl --file-list=./$1 --base-dir=$2 > desktop.$$.tmp
  dest=$3
  msguniq --to-code=UTF-8 -o desktop.$$ desktop.$$.tmp 2>/dev/null
  if test -f desktop.$$; then
    if test ! -f  $dest; then
      echo "File $dest is missing!"
      mv desktop.$$ $dest
    elif diff -q -I^\"POT-Creation-Date: desktop.$$ $dest > /dev/null; then
      rm -f desktop.$$
      touch $dest
    else
      mv desktop.$$ $dest
    fi
  fi
  rm -f desktop.$$ desktop.$$.tmp
}

dir=`dirname $0`
env_get_paths="get_paths"
l10n_module=""
if [ -n "${SCRIPTY_I18N_BRANCH}" ]; then
    env_get_paths="${env_get_paths}.${SCRIPTY_I18N_BRANCH}"
    if [[ "${SCRIPTY_I18N_BRANCH}" =~ ^trunk ]]; then
        l10n_module="l10n"
    fi
fi
. $dir/${env_get_paths}
releases="`list_modules $dir` ${l10n_module}"

for mod in $releases; do
    case "$mod" in
    l10n)
        extract_desktop all_files_$mod $BASEDIR/`get_path $mod` templates/messages/kconfigwidgets/l10n._desktop_.pot
        ;;
    *)
        extract_desktop all_files_$mod $BASEDIR/`get_path $mod` templates/messages/`get_po_path $mod`/`get_po_path $mod`._desktop_.pot
        ;;
    esac
done
